import { markdown } from 'markdown';
export default function($sanitize){
    function link(scope) {
        scope.$watch('data', newValue => {
            scope.convertedText = newValue ? $sanitize(markdown.toHTML(newValue)) : '';
        });
    }
    return {
        scope:{
            data: '='
        },
        template: `<div ng-bind-html = 'convertedText'></div>`,
        replace: true,
        link
    }
}
