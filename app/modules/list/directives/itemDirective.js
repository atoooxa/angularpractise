export default function(){
    return {
        scope:{
            remove: '&',
            edit: '&',
            info: '&',
            note: '='
        },
        template: `
        <li>
            <a ui-sref = 'note({id:note.id})'>{{note.data}}</a>
            <button ng-click = 'remove()'>&#10060</button>
            <button ng-click = 'edit()'>&#9999</button>
        </li>`,
        replace: true
    }
}
