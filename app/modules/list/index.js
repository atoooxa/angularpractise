import angular from 'angular';
import 'angular-sanitize';
import listActionsService from './services/listActionsService';
import descriptionService from './services/descriptionService';
import itemDirective from './directives/itemDirective';
import convertDirective from './directives/convertDirective';
import listCtrl from './controllers/listCtrl';
import noteCtrl from './controllers/noteCtrl';
import listFilter from './filters/listFilter';

const listModule = angular.module('listModule', ['ngSanitize']);

listModule.controller('listCtrl', listCtrl);
listModule.controller('noteCtrl', noteCtrl);

listModule.service('listActionsService', listActionsService);
listModule.service('descriptionService', descriptionService);

listModule.directive('item', itemDirective);
listModule.directive('convert', convertDirective);

listModule.filter('listFilter', listFilter);

export default listModule;
