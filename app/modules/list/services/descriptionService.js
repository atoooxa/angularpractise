class descriptionService {
    constructor($q, $window){
        this.$q = $q;
        this.localStorage = $window.localStorage;
    }
    checkId(id){
        return this.$q(resolve => {
            const storageList = JSON.parse(this.localStorage.getItem('list'));
            let exist;
            storageList.some(x => {
                if (x.id == id){
                    resolve('Exist.');
                    return true;
                }
            });
        });
    }
    getDescriptionData(id){
        return this.$q(resolve => {
            const storageList = JSON.parse(this.localStorage.getItem('list'));
            storageList.some(x => {
                if (x.id == id){
                    resolve({
                        author: x.author,
                        text: x.description,
                        title: x.data
                    });
                    return true;
                }
            });
        });
    }
    setDescriptionData(data){
        return this.$q((resolve, reject) => {
            const storageList = JSON.parse(this.localStorage.getItem('list'));
            const login = this.localStorage.getItem('loggedAs');
            if (login){
                let isAuthor;
                let editedElem;
                storageList.some((x, k) => {
                    if (x.id == data.id){
                        editedElem = k;
                        if (login == x.author){
                            isAuthor = true;
                        }
                        return true;
                    }
                });
                if (isAuthor){
                    storageList[editedElem].description = data.text;
                    this.localStorage.setItem('list', JSON.stringify(storageList));
                    resolve();
                }else{
                    reject(`You can't change others notes.`);
                }
            }else{
                reject('You should be logged in to change note.');
            }
        });
    }
}
descriptionService.$inject = ['$q','$window'];

export default descriptionService;
