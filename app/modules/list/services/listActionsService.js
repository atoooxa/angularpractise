class listActionsService {
    constructor($q, $window){
        this.$q = $q;
        this.localStorage = $window.localStorage;
    }
    getData(){
        return this.$q(resolve => {
            let id = this.localStorage.getItem('id');
            const login = this.localStorage.getItem('loggedAs');
            id = parseInt(id) || 0;
            resolve({
                id,
                list:JSON.parse(this.localStorage.getItem('list'))
            });
        });
    }
    addNote(data){
        return this.$q((resolve, reject) => {
            const login = this.localStorage.getItem('loggedAs');
            const id = data.id;
            if (login){
                data.author = login;
                let storageList = JSON.parse(this.localStorage.getItem('list'));
                storageList = storageList ? storageList.concat([data]) : [data];
                this.localStorage.setItem('list', JSON.stringify(storageList));
                this.localStorage.setItem('id', id);
                resolve(id);
            }else{
                reject('You should be logged in to add new notes.');
            }
        });
    }
    removeNote(elem){
        return this.$q((resolve, reject) => {
            let storageList = JSON.parse(this.localStorage.getItem('list'));
            const login = this.localStorage.getItem('loggedAs');
            if (login){
                let removedElem;
                let isAuthor;
                storageList.some((x, k) => {
                    if (x.id == elem.id){
                        removedElem = k;
                        if (login == x.author){
                            isAuthor = true;
                        }
                        return true;
                    }
                });
                if (isAuthor){
                    storageList.splice(removedElem, 1);
                    this.localStorage.setItem('list', JSON.stringify(storageList));
                    resolve(removedElem);
                }else{
                    reject(`You can't remove others notes.`);
                }
            }else{
                reject('You should be logged in to remove notes.');
            }
        });
    }
    editNote(elem){
        return this.$q((resolve, reject) => {
            let storageList = JSON.parse(this.localStorage.getItem('list'));
            const login = this.localStorage.getItem('loggedAs');
            if (login){
                let editedElem;
                let isAuthor;
                storageList.some((x, k) => {
                    if (x.id == elem.id){
                        editedElem = k;
                        if (login == x.author){
                            isAuthor = true;
                        }
                        return true;
                    }
                });
                if (isAuthor){
                    storageList[editedElem].data = elem.text;
                    this.localStorage.setItem('list', JSON.stringify(storageList));
                    resolve(editedElem);
                }else{
                    reject(`You can't edit others notes.`);
                }
            }else{
                reject('You should be logged in to edit notes.');
            }
        });
    }
}
listActionsService.$inject = ['$q','$window'];

export default listActionsService;
