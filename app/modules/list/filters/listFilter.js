export default function listFilter(){
    return (items, filter) =>{
        const filteredList = items.reduce((arr, x) => {
            if (x.data && ~x.data.indexOf(filter)){
                return arr.concat(x);
            }
            return arr;
        },[]);
        return filteredList;
    }
}
