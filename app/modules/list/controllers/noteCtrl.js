class noteCtrl {
    constructor (descriptionService, $stateParams){
        this.id = parseInt($stateParams.id);
        this.descriptionService = descriptionService;
        this.exist = false;
        descriptionService.checkId(this.id).then(() => {
            this.exist = true;
            descriptionService.getDescriptionData(this.id).then(data => {
                this.author = data.author;
                this.title = data.title;
                this.text = data.text || '';
            });
        });
    }
    saveDescription(){
        this.descriptionService.setDescriptionData({
            id: this.id,
            text: this.text
        }).then(() => {
            console.log('Saved.');
        }, data => {
            console.log(data);
        });
    }
}
noteCtrl.$inject = ['descriptionService', '$stateParams'];
export default noteCtrl;
