class listCtrl {
    constructor (listActionsService){
        this.listActionsService = listActionsService;
        this.listActionsService.getData().then((data) => {
            this.list = data.list || [];
            this.id = data.id;
        });
        this.filter = '';
    }
    addNote(){
        this.listActionsService.addNote({
            data: this.noteText,
            id: this.id + 1
        }).then(id => {
            this.id = id;
            this.list.push({
                data: this.noteText,
                id: id
            });
            this.noteText = '';
        }, data => {
            console.log(data);
        });
    }
    removeNote(elem){
        this.listActionsService.removeNote(elem).then(removedItem => {
            this.list.splice(removedItem, 1);
        }, data => {
            console.log(data);
        });
    }
    editNote(elem){
        this.listActionsService.editNote({
            id: elem.id,
            text: this.noteText
        }).then(editedItem => {
            this.list[editedItem].data = this.noteText;
            this.noteText = '';
        }, data => {
            console.log(data);
        });
    }
}
listCtrl.$inject = ['listActionsService'];
export default listCtrl;
