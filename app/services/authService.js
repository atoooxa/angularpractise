class authService {
    constructor($q, $window){
        this.$q = $q;
        this.localStorage = $window.localStorage;
    }
    login(data){
        const login = data.login;
        const password = data.password;
        return this.$q((resolve, reject) => {
            const users = JSON.parse(this.localStorage.getItem('users')) || [];
            if (!users.some(x => {
                if (x.login == login && x.password == password){
                    this.localStorage.setItem('loggedAs', login);
                    resolve({
                        login: login,
                        success: true
                    });
                    return true;
                }
            })){
                reject({
                    login,
                    success: false
                });
            }
        });
    }
    logout(data){
        return this.$q((resolve, reject) => {
            this.localStorage.removeItem('loggedAs');
            resolve();
        });
    }
    checkAuth(){
        return this.$q(resolve => {
            const login = this.localStorage.getItem('loggedAs');
            login && resolve(login);
        });
    }
}
authService.$inject = ['$q','$window'];

export default authService;
