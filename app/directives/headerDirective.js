import _ from 'lodash';
console.log(_.compact([1,'',2,NaN,3,4]));
export default function(authService){
    function link(scope) {
        const ctrl = scope.ctrl;
        authService.checkAuth().then(data => {
            if (data){
                scope.status = `Login as ${data}`;
                ctrl.user = data;
            }
        });
        scope.login = () => {
            authService.login({
                login: ctrl.userLogin,
                password: ctrl.userPassword
            }).then(data => {
                const login = data.login;
                ctrl.user = login;
                scope.status = `Login as ${login}`;
            }, () => {
                scope.status = 'Login failed';
            });
            ctrl.userLogin = '';
            ctrl.userPassword = '';
        };
        scope.logout = () => {
            authService.logout().then(data => {
                delete(ctrl.user);
                scope.status = '';
            });
        };
    }
    return {
        controller:['$scope', ($scope) => {}],
        controllerAs: 'ctrl',
        template: `
        <div class = 'header arial'>
            <span ng-if = "!ctrl.user">
                <input placeholder = 'Login' ng-model = 'ctrl.userLogin'>
                <input placeholder = 'Password' type = 'password' ng-model = 'ctrl.userPassword'>
                <button ng-click = "login()">Login</button>
            </span>
            <span>{{status}}</span>
            <button ng-if = "ctrl.user" ng-click = 'logout()'>Logout</button>
        </div>`,
        replace: true,
        link
    }
}
