import angular from 'angular';
import 'angular-ui-router';
import '../css/index.css';
import '../modules/list';
import mainHtml from '../partials/main.html';
import noteHtml from '../partials/note.html';
import headerDirective from '../directives/headerDirective';
import authService from '../services/authService';



const App = angular.module('App', ['ui.router', 'listModule']);
App.directive('header', headerDirective);
App.service('authService', authService);

App.config(($stateProvider, $urlRouterProvider, $locationProvider) => {
    $stateProvider
        .state('home', {
            url: '/',
            template: mainHtml,
            controller: 'listCtrl',
            controllerAs: 'listCtrl'
        }).state('note', {
            url: '/note/:id',
            template: noteHtml,
            controller: 'noteCtrl',
            controllerAs: 'noteCtrl'
        });
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode({ enabled: true, requireBase: false });
});
